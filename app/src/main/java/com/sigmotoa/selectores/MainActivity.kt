package com.sigmotoa.selectores

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*

var rb1:RadioButton?=null
var rb2:RadioButton?=null
var rb3:RadioButton?=null
var rb4:RadioButton?=null


class MainActivity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rtb:RatingBar=findViewById(R.id.ratingBar)
        rtb.setOnRatingBarChangeListener { ratingBar, fl, b -> msg("Calificación $fl") }

        var rg:RadioGroup = findViewById(R.id.rg)
        rb1 = findViewById(R.id.rb1)
        rb2 = findViewById(R.id.rb2)
        rb3 = findViewById(R.id.rb3)
        rb4 = findViewById(R.id.rb4)
        rg?.setOnCheckedChangeListener(this)

    }

    fun msg (number:String){
        val tv:TextView = findViewById(R.id.et1)
        tv.setText("Estás en $number")
        var msg=Toast.makeText(this, "Radio $number", Toast.LENGTH_SHORT)
        msg.setGravity(Gravity.TOP,0,0)
        msg.show()
    }
    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton){
            val checked = view.isChecked
            when(view.id){

                R.id.rb5-> if (checked)
                {msg("Botón 5 del grupo 2")}

                R.id.rb6->if (checked)
                {msg("Botón 6 del grupo 2")}

                R.id.rb7-> if (checked)
                {msg("Botón 7 del grupo 2")}

                R.id.rb8-> if (checked)
                {msg("Botón 8 del grupo 2")}

                R.id.rb9-> if (checked)
                {msg("Botón 9 del grupo 2")}

                R.id.rb10-> if (checked)
                {msg("Botón 10 del grupo 2")}

                R.id.rb11-> if (checked)
                {msg("Botón 11 del grupo 2")}

                R.id.rb12-> if (checked)
                {msg("Botón 12 del grupo 2")}

            }
        }
    }

    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
        when(p1){
            rb1?.id ->msg("Botón 1")
            rb2?.id ->msg("Botón 2")
            rb3?.id ->msg("Botón 3")
            rb4?.id ->msg("Botón 4")
        }
    }
}







